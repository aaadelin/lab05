package lab05.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://192.168.0.199:8080")
public class LoginPage extends PageObject {

    @FindBy(id="username")
    private WebElementFacade usernameInput;

    @FindBy(id="password")
    private WebElementFacade passwordInput;

    @FindBy(name="loginButton")
    private WebElementFacade loginButton;

    public void enterUsername(String keyword) {
        usernameInput.type(keyword);
    }

    public void enterPassword(String keyword) {
        passwordInput.type(keyword);
    }

    public void pressLoginButton() {
        loginButton.click();
    }

}
