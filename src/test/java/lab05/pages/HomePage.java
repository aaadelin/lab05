package lab05.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@DefaultUrl("http://192.168.0.199:8080")
public class HomePage extends PageObject {

    public Boolean profilePictureIsPresent() {
        return find(By.id("profilePicture")).isPresent();
    }

    public void pressProjectsButton() {
        this.getDriver().manage().window().maximize();
        Optional<WebElementFacade> first = findAll(By.tagName("a")).stream().filter(e -> e.getText().equalsIgnoreCase("projects")).findFirst();
        first.ifPresent(WebElementFacade::click);
    }

    public void pressLogoutButton() {
        this.getDriver().manage().window().maximize();
        Optional<WebElementFacade> first = findAll(By.tagName("a")).stream().filter(e -> e.getText().equalsIgnoreCase("log out")).findFirst();
        first.ifPresent(WebElementFacade::click);
    }

    public void pressCreateProjectButton() {
        WebElementFacade createProject = find(By.name("createProject"));
        createProject.click();
    }

    public void completeName(String name){
        WebElementFacade nameInput = find(By.id("name"));
        nameInput.type(name);
    }

    public void completeDescription(String description){
        WebElementFacade descriptionInput = find(By.name("descriptionArea"));
        descriptionInput.type(description);
    }

    public void completeForm(String name, String description, String deadline) {
        completeName(name);
        completeDescription(description);
        completeDeadline(deadline);
    }

    private void completeDeadline(String deadline) {
        WebElementFacade deadlineInput = find(By.name("deadlineInput"));
        deadlineInput.type(deadline);
    }

    public void submitForm() {
        find(By.id("saveButton")).click();
    }

    public List<String> getAllProjects() {
        return findAll(By.tagName("h4")).stream().map(WebElementFacade::getText).collect(Collectors.toList());
    }

    public void refresh() {
        this.getDriver().navigate().refresh();
    }

    public void pressProjectOptionsButton() {
        List<WebElementFacade> menus = findAll(By.id("showMenu"));
        WebElementFacade webElementFacade = menus.get(menus.size() - 1);
        webElementFacade.click();
    }

    public void pressArchiveProjectButton() {
        List<WebElementFacade> menus = findAll(By.id("archiveProject"));
        WebElementFacade webElementFacade = menus.get(menus.size() - 1);
        webElementFacade.click();
    }

    public int getProjectsCount() {
        return (int) findAll(By.tagName("h4")).stream().map(WebElementFacade::getText).count();
    }
}
