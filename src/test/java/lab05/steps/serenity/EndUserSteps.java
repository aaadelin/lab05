package lab05.steps.serenity;

import lab05.pages.HomePage;
import lab05.pages.LoginPage;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class EndUserSteps {

    LoginPage loginPage;
    HomePage homePage;

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Step
    public void is_the_login_page() {
        loginPage.open();
    }

    @Step
    public void entersUsername(String username){
        loginPage.enterUsername(username);
    }

    @Step
    public void entersPassword(String password){
        loginPage.enterPassword(password);
    }

    @Step
    public void entersCredentials(String username, String password){
        entersUsername(username);
        entersPassword(password);
    }

    @Step
    public void pressesLoginButton() {
        loginPage.pressLoginButton();
    }

    @Step
    public void shouldBeLoggedIn(boolean isvalid) {
        homePage.open();
        if(isvalid){
            assert homePage.profilePictureIsPresent();
        }else {
            assert !homePage.profilePictureIsPresent();
        }
    }


    @Step
    public void pressesLogoutButton() {
        homePage.pressLogoutButton();
    }


    @Step
    public int openProjectsPage() {
        homePage.pressProjectsButton();
        return homePage.getProjectsCount();
    }


    @Step
    public void pressCreateProjectButton() {
        homePage.pressCreateProjectButton();
    }


    @Step
    public void completeForm(String name, String description, String deadline) {
        homePage.completeForm(name, description, deadline);
    }


    @Step
    public void submitForm() {
        homePage.submitForm();
    }


    @Step
    public void shouldSeeNewProject(String name) {
        homePage.refresh();
        List<String> projects = homePage.getAllProjects();
        assert projects.contains(name);
    }


    @Step
    public void pressOptionsButton() {
        homePage.pressProjectOptionsButton();
    }


    @Step
    public void pressArchiveButton() {
        homePage.pressArchiveProjectButton();
    }


    @Step
    public void shouldSeeLessProjects(int projectsCount) {
        assert projectsCount > homePage.getProjectsCount();
    }
}