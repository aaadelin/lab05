package lab05.features.search;


import lab05.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/addProjectScenario.csv")
public class ScenarioBasedTesting {


    @Steps
    public EndUserSteps anna;

    public String username;
    public String password;
    public String name;
    public String description;
    public String deadline;

    public void authenticateUser(){
        anna.is_the_login_page();
        anna.entersCredentials(username, password);
        anna.pressesLoginButton();
        anna.shouldBeLoggedIn(true);
    }

    public void createProject(){
        anna.openProjectsPage();
        anna.pressCreateProjectButton();
        anna.completeForm(name, description, deadline);
        anna.submitForm();
        anna.shouldSeeNewProject(name);
    }

    public void archiveProject(){
        int projectsCount = anna.openProjectsPage();
        anna.pressOptionsButton();
        anna.pressArchiveButton();
        anna.shouldSeeLessProjects(projectsCount);
    }

    public void logout(){
        anna.pressesLogoutButton();
        anna.shouldBeLoggedIn(false);
    }

    @Test
    public void scenario(){
        authenticateUser();
        createProject();
        archiveProject();
        logout();
    }
}

