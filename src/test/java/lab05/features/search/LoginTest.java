package lab05.features.search;

import lab05.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/loginData.csv")
public class LoginTest {


    @Steps
    public EndUserSteps anna;

    public String username;
    public String password;
    public Boolean isvalid;

    @Test
    public void userAuthenticationValidation(){
        anna.is_the_login_page();
        anna.entersCredentials(username, password);
        anna.pressesLoginButton();
        anna.shouldBeLoggedIn(isvalid);
    }

}
